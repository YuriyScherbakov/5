﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ErrorGenerator eg1 = new ErrorGenerator();
            ErrorGenerator eg2 = new ErrorGenerator();
            ErrorWatcher errWatcher = new ErrorWatcher();

            Thread thread_1 = new Thread(eg1.Error);
            Thread thread_2 = new Thread(eg2.Error);

            thread_1.IsBackground = true;
            thread_2.IsBackground = true;

            thread_1.Name = "Эмметт Браун";
            thread_2.Name = "Марти Макфлай";



            eg1.ErrorOccurred += errWatcher.OnErrorOccured;
            eg2.ErrorOccurred += errWatcher.OnErrorOccured;
            MyForm form = new MyForm(errWatcher);

            form.ErrorGenerateStartPermitions += eg1.StartPermitions;
            form.ErrorGenerateStopPermitions += eg1.StopPermitions;
            form.ErrorGenerateStartPermitions += eg2.StartPermitions;
            form.ErrorGenerateStopPermitions += eg2.StopPermitions;

            thread_1.Start();
            thread_2.Start();

            Application.Run(form);

        }


    }
}
