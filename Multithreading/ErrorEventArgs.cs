﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multithreading
{
    public class ErrorEventArgs : EventArgs
    {
        string error;
        string threadName;
        public string _error
        {
            get
            {
                return this.error;
            }
            set
            {
                this.error = value;
            }
        }
        public string ThreadName
        {
            get
            {
                return this.threadName;
            }
            set
            {
                this.threadName = value;
            }
        }
    }
}
