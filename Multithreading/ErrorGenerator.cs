﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading
{
    class ErrorGenerator
    {
        public delegate void ErrorOccurredEventHandler(object source,ErrorEventArgs err);
        public event ErrorOccurredEventHandler ErrorOccurred;
        Thread currentThread;

        protected virtual void OnErrorOccurred(Multithreading.ErrorEventArgs err)
        {
            if ( ErrorOccurred != null )
            {
                ErrorOccurred(this,err);
            }

        }

        bool doGenerateFlag = false;

        public void StartPermitions(object sourse,EventArgs args)
        {
            if ( sourse.ToString() != this.currentThread.Name )
            {
                return;
            }
            else
            {
                this.doGenerateFlag = true;
            }

        }
        public void StopPermitions(object sourse,EventArgs args)
        {

            if ( sourse.ToString() != this.currentThread.Name )
            {
                return;
            }
            else
            {
                this.doGenerateFlag = false;
            }

        }

        public void Error()
        {
            if ( currentThread == null )
            {
                currentThread = Thread.CurrentThread;
            }
            while ( true )
            {
                //Thread.Sleep(1000);
                Random rand = new Random(DateTime.Now.Millisecond);

                if ( doGenerateFlag )
                {

                    try
                    {

                        switch ( rand.Next(1,6) )
                        {
                            case 1:
                                throw new DivideByZeroException();
                                break;
                            case 2:
                                throw new System.IO.FileNotFoundException();
                                break;
                            case 3:
                                throw new OutOfMemoryException();
                                break;
                            case 4:
                                throw new StackOverflowException();
                                break;

                            default:
                                throw new Exception("Что-то пошло не так...");
                                break;

                        }
                    }
                    catch ( Exception e )
                    {

                        Multithreading.ErrorEventArgs err = new Multithreading.ErrorEventArgs();
                        err.ThreadName = Thread.CurrentThread.Name;

                        err._error = e.Message;
                        OnErrorOccurred(err);
                    }


                }
            }

        }

    }
}
