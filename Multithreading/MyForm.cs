﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading
{
    public partial class MyForm : Form
    {
        public event EventHandler ErrorGenerateStopPermitions;
        public event EventHandler ErrorGenerateStartPermitions;

        public MyForm(ErrorWatcher errWatcher)
        {
            errWatcher.ErrorAdded += ErrWatcher_ErrorAdded;
            InitializeComponent();
        }



        private void ErrWatcher_ErrorAdded(object sender,Multithreading.ErrorReport errReport)
        {
            Action action = () =>
            {

                this.textBox1.AppendText("Поток " +
              errReport.errEvArgs.ThreadName.ToString() +
              " вызвал исключение " +
              errReport.errEvArgs._error.ToString());
                this.textBox1.AppendText(Environment.NewLine);
                this.textBox2.Text = errReport.currentErrorsAmount.ToString();

            };
                    Invoke(action);
          
        }

        protected virtual void OnErrorGenerateStartPermitions(object sender)
        {
            if ( ErrorGenerateStartPermitions != null )
            {
                ErrorGenerateStartPermitions(sender,EventArgs.Empty);
            }

        }
        protected virtual void OnErrorGenerateStopPermitions(object sender)
        {
            if ( ErrorGenerateStopPermitions != null )
            {
                ErrorGenerateStopPermitions(sender,EventArgs.Empty);
            }

        }

       
        private void MyForm_Load(object sender,EventArgs e)
        {

        }

        private void buttonStartMarty_Click(object sender,EventArgs e)
        {
            OnErrorGenerateStartPermitions("Марти Макфлай");
        }
        private void buttonStopMarty_Click(object sender,EventArgs e)
        {
            OnErrorGenerateStopPermitions( "Марти Макфлай" );
        }


        private void buttonStartDoc_Click(object sender,EventArgs e)
        {
            OnErrorGenerateStartPermitions("Эмметт Браун");
        }
        private void buttonStopDoc_Click(object sender,EventArgs e)
        {
            OnErrorGenerateStopPermitions("Эмметт Браун");
        }

        private void MyForm_FormClosing(object sender,FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
