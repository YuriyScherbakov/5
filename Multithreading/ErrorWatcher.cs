﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading
{
    public class ErrorWatcher
    {
        static Queue<ErrorEventArgs> errorQueue = new Queue<Multithreading.ErrorEventArgs>();

        public delegate void ErrorAddedEventHandler(object source,ErrorReport errReport);
        public event ErrorAddedEventHandler ErrorAdded;
        static object _lock = new object();

        public void OnErrorOccured(object source,Multithreading.ErrorEventArgs err)
        {


            Monitor.Enter(_lock);
            try
            {
                errorQueue.Enqueue(err);
                ErrorReport errReport = new ErrorReport();
                errReport.errEvArgs = err;
                errReport.currentErrorsAmount = errorQueue.Count();

                OnErrorAdded(errReport);

            }
            finally
            {
                Monitor.Exit(_lock);
            }
        }

        protected virtual void OnErrorAdded(ErrorReport errReport)
        {

            if ( ErrorAdded != null )
            {

                ErrorAdded(this,errReport);
            }

        }
    }
}
