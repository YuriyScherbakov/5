﻿namespace Multithreading
{
    partial class MyForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonStartMarty = new System.Windows.Forms.Button();
            this.buttonStopMarty = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonStopDoc = new System.Windows.Forms.Button();
            this.buttonStartDoc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(706, 256);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(655, 285);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(63, 20);
            this.textBox2.TabIndex = 2;
            // 
            // buttonStartMarty
            // 
            this.buttonStartMarty.Location = new System.Drawing.Point(29, 27);
            this.buttonStartMarty.Name = "buttonStartMarty";
            this.buttonStartMarty.Size = new System.Drawing.Size(75, 23);
            this.buttonStartMarty.TabIndex = 4;
            this.buttonStartMarty.Text = "Старт";
            this.buttonStartMarty.UseVisualStyleBackColor = true;
            this.buttonStartMarty.Click += new System.EventHandler(this.buttonStartMarty_Click);
            // 
            // buttonStopMarty
            // 
            this.buttonStopMarty.Location = new System.Drawing.Point(29, 57);
            this.buttonStopMarty.Name = "buttonStopMarty";
            this.buttonStopMarty.Size = new System.Drawing.Size(75, 23);
            this.buttonStopMarty.TabIndex = 5;
            this.buttonStopMarty.Text = "Стоп";
            this.buttonStopMarty.UseVisualStyleBackColor = true;
            this.buttonStopMarty.Click += new System.EventHandler(this.buttonStopMarty_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonStopMarty);
            this.groupBox1.Controls.Add(this.buttonStartMarty);
            this.groupBox1.Location = new System.Drawing.Point(12, 285);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 99);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Марти Макфлай";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonStopDoc);
            this.groupBox2.Controls.Add(this.buttonStartDoc);
            this.groupBox2.Location = new System.Drawing.Point(168, 285);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(141, 99);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Эмметт Браун";
            // 
            // buttonStopDoc
            // 
            this.buttonStopDoc.Location = new System.Drawing.Point(29, 57);
            this.buttonStopDoc.Name = "buttonStopDoc";
            this.buttonStopDoc.Size = new System.Drawing.Size(75, 23);
            this.buttonStopDoc.TabIndex = 5;
            this.buttonStopDoc.Text = "Стоп";
            this.buttonStopDoc.UseVisualStyleBackColor = true;
            this.buttonStopDoc.Click += new System.EventHandler(this.buttonStopDoc_Click);
            // 
            // buttonStartDoc
            // 
            this.buttonStartDoc.Location = new System.Drawing.Point(29, 27);
            this.buttonStartDoc.Name = "buttonStartDoc";
            this.buttonStartDoc.Size = new System.Drawing.Size(75, 23);
            this.buttonStartDoc.TabIndex = 4;
            this.buttonStartDoc.Text = "Старт";
            this.buttonStartDoc.UseVisualStyleBackColor = true;
            this.buttonStartDoc.Click += new System.EventHandler(this.buttonStartDoc_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(466, 292);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Количество исключений в очереди";
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 400);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MyForm";
            this.Text = "ДЗ 5 Многопоточность";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MyForm_FormClosing);
            this.Load += new System.EventHandler(this.MyForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonStartMarty;
        private System.Windows.Forms.Button buttonStopMarty;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonStopDoc;
        private System.Windows.Forms.Button buttonStartDoc;
        private System.Windows.Forms.Label label1;
    }
}

